import cv2.cv2 as cv2
import sys
import numpy as np

camera = cv2.VideoCapture('/home/ml/Videos/improved_model_negative_common_0.avi')

ret, frame = camera.read()

if not ret:
    print("Unable to read camera ") 
    exit()

frame_count = 0

border = 1
threshold_value = 7

Crop_Size = 224

img_size = 224
BLACK = [0, 0, 0]   


def get_pill(frame, cnt, crop=Crop_Size, resize=img_size):
    hull = cv2.convexHull(cnt)
    M = cv2.moments(hull)
    c_x = int(M["m10"] / M["m00"])
    c_y = int(M["m01"] / M["m00"])
    mask = np.zeros(frame.shape[:2], dtype=frame.dtype)
    mask = cv2.drawContours(mask, [hull], -1, 255, -1)
    out = np.zeros_like(frame)
    out[mask == 255] = frame[mask == 255]
    out = cv2.copyMakeBorder(out, crop, crop, crop, crop, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    c_x = c_x + crop
    c_y = c_y + crop
    pill = out[c_y - int(crop / 2):c_y + int(crop / 2), c_x - int(crop / 2):c_x + int(crop / 2)]
    pill = cv2.resize(pill, (resize, resize), cv2.INTER_AREA)
    return pill


def ExtractThreshold(frame, min_thresh=threshold_value):

    blur = cv2.medianBlur(frame, 5, 0)
    gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, min_thresh, 255, cv2.THRESH_BINARY)
    morphological_kernel = np.ones((3, 3), np.uint8)
    eroded_frame = cv2.erode(thresh, morphological_kernel, iterations=2)
    dilated_frame = cv2.dilate(eroded_frame, morphological_kernel, iterations=2)
    return dilated_frame


def extract_contours(image, contour, border=border):

    top_left_x, top_left_y, width, height = cv2.boundingRect(contour)
    pill_flag = 0

    if top_left_y - border >= 0 and top_left_y + height + border <= 720 and top_left_x - border >= 0 and top_left_x + width + border <= 1280:

        resizedImage = get_pill(image, contour)
        pill_flag = 1
    else:
        resizedImage = None
        pill_flag = 0
        pass

    return pill_flag, resizedImage


ret, background_frame = camera.read()

while frame_count < 20:
    frame_count = frame_count + 1
    ret, background_frame = camera.read()

background_gray_frame = cv2.cvtColor(background_frame, cv2.COLOR_RGB2GRAY)

while True:
    ret, frame = camera.read()
    if not ret:
        print("Unable to read camera ")
        break

    sub_frame = cv2.subtract(frame, background_frame)
    dilated_frame = ExtractThreshold(sub_frame)
    contours = cv2.findContours(dilated_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]

    pill_cropped = frame
    if len(contours) > 0:
        for i in range(len(contours)):
            if cv2.contourArea(contours[i]) > 3000:
                pill_flag, resized_pill = extract_contours(sub_frame, contours[i])
                if pill_flag:
                    cv2.imwrite("/home/ml/PycharmProjects/ImageMatching/dataset/negative/" + str(frame_count) +'_' + str(cv2.contourArea(contours[i])) + ".bmp", resized_pill)
                    print("Found Pill")

    frame_count = frame_count + 1
