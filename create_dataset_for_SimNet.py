import numpy as np

p = np.load('./Slope/Advance_2_RESNET.npy')
n = np.load('./Slope/Anomaly_RESNET.npy')

p_label = np.ones(p.shape[0])
n_label = np.zeros(n.shape[0])

q = np.vstack((p, n))
q_label = np.hstack((p_label, n_label))

print(p.shape, p_label.shape)
print(n.shape, n_label.shape)
print(q.shape, q_label.shape)

np.save('pill_data_RESNET', q)
np.save('pill_label_RESNET', q_label)
