import numpy.random as rng
from keras import backend as K, metrics
from keras.layers import Input, Conv2D, Dense, Flatten, MaxPooling2D, Add, Lambda, Concatenate, Dropout
from keras.models import Model, Sequential
from keras.optimizers import SGD, Adam
from keras.regularizers import l2
import random
from sklearn.model_selection import train_test_split
import numpy as np

np.random.seed(1337)


def load_data():
    pill_data = np.load('pill_data_images.npy')
    pill_label = np.load('pill_label_images.npy')
    x_train, x_test, y_train, y_test = train_test_split(pill_data, pill_label, test_size=0.33, random_state=42)
    return (x_train, y_train), (x_test, y_test)


def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True) + 0.000001)


def distance_output_shape(shapes):
    shape1, shape2 = shapes
    return shape1[0], 1


def contrastive_loss(y_true, y_pred):
    margin = 1
    return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))


def compute_accuracy(predictions, labels):
    return labels[predictions.ravel() < 0.5].mean()


def accuracy(y_true, y_pred):
    threshold = 0.5
    return K.mean(K.equal(y_true, K.cast(y_pred < threshold, y_true.dtype)))


def pair_generator(numbers):
    used_pairs = set()

    while True:
        pair = random.sample(numbers, 2)
        pair = tuple(sorted(pair))
        if pair not in used_pairs:
            used_pairs.add(pair)
            yield pair


def create_pairs(pill_data, pill_labels, samples):
    positive_pairs = []
    negative_pairs = []
    labels_indices = pill_labels.tolist()
    positive_indices = [i for i, x in enumerate(labels_indices) if x == 1]
    negative_indices = [i for i, x in enumerate(labels_indices) if x == 0]
    positive_gen = pair_generator(positive_indices)
    negative_gen = pair_generator(negative_indices)
    for i in range(samples):
        pair = positive_gen.__next__()
        positive_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))

    for j in range(samples):
        pair = negative_gen.__next__()
        negative_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))

    positive = np.array(positive_pairs)
    negative = np.array(negative_pairs)
    positive_labels = np.ones(positive.shape[0])
    negative_labels = np.zeros(negative.shape[0])
    pill_pair_label = np.hstack((positive_labels, negative_labels))
    pill_pair_data = np.vstack((positive, negative))
    print(pill_pair_data.shape, pill_pair_label.shape)
    return pill_pair_data, pill_pair_label


def feature_net(input_shape):
    convnet = Sequential()
    convnet.add(Conv2D(32, (3, 3), activation='relu', input_shape=input_shape))
    convnet.add(MaxPooling2D())
    convnet.add(Conv2D(64, (3, 3), activation='relu'))
    convnet.add(MaxPooling2D())
    convnet.add(Conv2D(128, (3, 2), activation='relu'))
    convnet.add(MaxPooling2D())
    convnet.add(Conv2D(256, (3, 3), activation='relu'))
    convnet.add(Flatten())
    convnet.add(Dense(512, activation="relu"))
    convnet.add(Dropout(0.1))
    convnet.add(Dense(256, activation="relu"))
    convnet.add(Dropout(0.5))
    convnet.add(Dense(128, activation="relu"))

    return convnet


def SimNet_model(input_shape):
    left_input = Input(input_shape)
    right_input = Input(input_shape)

    convnet = feature_net(input_shape)

    encoded_l = convnet(left_input)
    encoded_r = convnet(right_input)

    merged = Concatenate(axis=1)([encoded_l, encoded_r])
    prediction = Dense(1, activation='sigmoid', name="prediction")(merged)

    distance = Lambda(euclidean_distance, output_shape=distance_output_shape, name="distance")([encoded_l, encoded_r])

    sim_Net = Model(input=[left_input, right_input], output=[prediction, distance])

    return sim_Net


def main():
    (X_train, y_train), (X_test, y_test) = load_data()
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    input_dim = (224, 224, 3)
    nb_epoch = 10

    model = SimNet_model(input_dim)

    tr_pairs, tr_y = create_pairs(X_train, y_train, 500)
    te_pairs, te_y = create_pairs(X_test, y_test, 50)

    losses = {
        "prediction": "binary_crossentropy",
        "distance": contrastive_loss,
    }

    lossWeights = {"prediction": 1.0, "distance": 1.0}

    optimizer = Adam()

    model.compile(loss=losses, loss_weights=lossWeights,  optimizer=optimizer, metrics=[metrics.binary_accuracy, accuracy])
    model.fit([tr_pairs[:, :224], tr_pairs[:, 224:]], [tr_y, tr_y],
              validation_data=([te_pairs[:, :224], te_pairs[:, 224:]], [te_y, te_y]),
              batch_size=10,
              nb_epoch=nb_epoch)

    model.save('pill_simnet_conv.h5')

    pred = model.predict([tr_pairs[:, :224], tr_pairs[:, 224:]])
    tr_acc = compute_accuracy(pred, tr_y)
    pred = model.predict([te_pairs[:, :224], te_pairs[:, 224:]])
    te_acc = compute_accuracy(pred, te_y)

    print('* Accuracy on training set: %0.2f%%' % (100 * tr_acc))
    print('* Accuracy on test set: %0.2f%%' % (100 * te_acc))


if __name__ == "__main__":
    main()
